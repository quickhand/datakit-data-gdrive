import mimetypes
import sys
import os
import io
import logging
import hashlib
import dateutil.parser
import webbrowser

from httplib2 import Http

from oauth2client.client import OAuth2WebServerFlow
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import OAuth2Credentials
from oauth2client.tools import ClientRedirectHandler
from oauth2client.tools import ClientRedirectServer

from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload, MediaIoBaseUpload

from progress_bar import InitBar

logger = logging.getLogger()
logger.setLevel(logging.WARNING)


class AuthError(Exception):
  """Base error for authentication/authorization errors."""


class InvalidCredentialsError(IOError):
  """Error trying to read credentials file."""


class AuthenticationRejected(AuthError):
  """User rejected authentication."""


class AuthenticationError(AuthError):
  """General authentication error."""


OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive'


class GDrive:
    """
    A limited interface to GDRIVE.

    """

    # Public
    def get_credentials_from_obj(self,credentials_obj):
        kwargs = {
            "access_token": credentials_obj['access_token'],
            "client_id": credentials_obj['client_id'],
            "client_secret": credentials_obj['client_secret'],
            "refresh_token": credentials_obj['refresh_token'],
            "token_expiry": dateutil.parser.parse(credentials_obj['token_expiry']),
            "token_uri": 'https://oauth2.googleapis.com/token',
            "user_agent": 'datakit_checker/1.0',
            "scopes": [OAUTH_SCOPE]
        }
        return OAuth2Credentials(**kwargs)

    def get_obj_from_credentials(self,credentials):
        credentials_dict = vars(credentials)
        return {
            'client_id': credentials_dict['client_id'],
            'client_secret': credentials_dict['client_secret'],
            'access_token': credentials_dict['access_token'],
            'refresh_token': credentials_dict['refresh_token'],
            'token_expiry': credentials_dict['token_expiry'].isoformat()
        }

    def auth(self, credentials_obj = {}):
        
        if 'access_token' in credentials_obj:
            credentials = self.get_credentials_from_obj(credentials_obj)
            if not credentials.access_token_expired:
                return self.get_obj_from_credentials(credentials)
            else:
                try:
                    credentials.refresh(Http())
                    return self.get_obj_from_credentials(credentials)
                except AccessTokenRefreshError:
                    return self.new_auth(credentials_obj['client_id'], credentials_obj['client_secret'])
        else:
            return self.new_auth(credentials_obj['client_id'], credentials_obj['client_secret'])


    def new_auth(self, client_id, client_secret):
        flow = OAuth2WebServerFlow(
            client_id,
            client_secret,
            OAUTH_SCOPE,)
        host_name = "localhost"
        port_numbers = [8090, 8080]
        success = False
        port_number = 0
        for port in port_numbers:
            port_number = port
            try:
                httpd = ClientRedirectServer((host_name, port), ClientRedirectHandler)
            except:
                pass
            else:
                success = True
                break
        if success:
            oauth_callback = "http://{}:{}/".format(host_name, port_number)
        else:
            print('Local web server launch unsucessful.')
            raise AuthenticationError()
        flow.redirect_uri = oauth_callback
        authorize_url = flow.step1_get_authorize_url()
        webbrowser.open(authorize_url, new=1, autoraise=True)
        print('Your browser has been opened to:\n\n' + authorize_url)
        httpd.handle_request()
        if 'error' in httpd.query_params:
            print('Authentication request was rejected')
            raise AuthenticationRejected('User rejected authentication')
        if 'code' in httpd.query_params:
            print(httpd.query_params['code'])
            credentials = flow.step2_exchange(httpd.query_params['code'])
            return self.get_obj_from_credentials(credentials)
        else:
            print('Failed to find "code" in the query parameters of the redirect.')
            raise AuthenticationError('No code found in redirect')
    
    def get_service(self,credentials):
        http = Http()
        credentials.authorize(http)
        return build('drive', 'v3', credentials=credentials, cache_discovery=False)
    
    def get_drives(self, drive_service):
        drive_list = []
        #try:
        page_token = None
        response = drive_service.drives().list(
            pageToken=page_token,
            fields='nextPageToken, drives(id, name)'
        ).execute()
        
        for drive in response.get('drives',[]):
            drive_list.append({
                "id": drive.get("id"),
                "name": drive.get("name")
            })
        # except Exception as e:
        #     print(e)
        return drive_list



    def get_file(self, drive_service, name, mime_type, parent_id=None):
        q = "name='{}' and mimeType='{}'".format(name, mime_type)
        if parent_id != None:
            q+= " and '{}' in parents".format(parent_id)
        try:
            page_token = None
            while True:
                response = drive_service.files().list(
                    q=q,
                    spaces='drive',
                    fields='nextPageToken, files(id, name, parents, trashed, md5Checksum)',
                    pageToken=page_token,
                    supportsAllDrives=True
                ).execute()
                for file in response.get('files', []):
                    if file.get("trashed"):
                        continue
                    if parent_id == None:
                        return (file.get('id'),file.get('md5Checksum'))
                    elif parent_id in file.get("parents"):
                        return (file.get('id'),file.get('md5Checksum'))
                    
                page_token = response.get('nextPageToken', None)
                if page_token is None:
                    break
        except Exception as e:
            print(e)
        return (None,None)     


    def create_dir(self, drive_service, dir_name, parent_id = None):

        file_id, md5 = self.get_file(drive_service, dir_name, "application/vnd.google-apps.folder", parent_id)
        if file_id != None:
            return file_id

        file_metadata = {
            'name': dir_name,
            'mimeType': 'application/vnd.google-apps.folder',
        }
        if parent_id != None:
            file_metadata["parents"] = [parent_id]

        file = drive_service.files().create(
            body=file_metadata,
            fields='id',
            supportsAllDrives=True
        ).execute()
        return file.get("id")

    def create_file(self, drive_service, file_path, name, parent_id=None):
        mime_type, encoding = mimetypes.guess_type(name)
        if mime_type == None:
            mime_type = "text/*"
        file_id, md5 = self.get_file(drive_service, name, mime_type, parent_id)

        if file_id != None:
            if self.get_local_file_hash(file_path) == md5:
                print("  {} exists online and is unchanged, skipping".format(file_path))
                return file_id
            else:
                msg = "{} exists online but has changed, updating".format(file_path)
                #media = MediaFileUpload(file_path, mimetype=mime_type)
                #drive_service.files().update(fileId = file_id, media_body=media, fields='id').execute()

                
                if os.stat(file_path).st_size == 0:
                    print("  "+msg)
                    media = MediaFileUpload(file_path, mimetype=mime_type)
                    drive_service.files().update(
                        fileId = file_id,
                        media_body=media,
                        fields='id',
                        supportsAllDrives=True
                    ).execute()
                else:
                    with io.FileIO(file_path, mode="rb") as fh:
                        media = MediaIoBaseUpload(
                            fh,
                            mimetype=mime_type,
                            chunksize=1024*1024, 
                            resumable=True
                        )
                        request = drive_service.files().update(
                            fileId = file_id,
                            media_body=media,
                            fields='id',
                            supportsAllDrives=True
                        )
                        
                        response = None
                        pbar = InitBar(msg)
                        while response is None:
                            status, response = request.next_chunk(num_retries=3)
                            if status:
                                pbar(round(status.progress()*100))
                        del pbar
        else:
            msg = "{} is new, uploading".format(file_path)
            file_metadata = {
                'name': name,
                'mimeType': mime_type,
            }
            if parent_id != None:
                file_metadata["parents"] = [parent_id]
            # media = MediaFileUpload(file_path, mimetype=mime_type)
            # drive_service.files().create(
            #     body=file_metadata,
            #     media_body=media,
            #     fields='id'
            # ).execute()
            if os.stat(file_path).st_size == 0:
                print("  "+msg)
                drive_service.files().create(
                    body=file_metadata,
                    fields='id',
                    supportsAllDrives=True
                ).execute()
            else:
                with io.FileIO(file_path, mode="rb") as fh:
                    media = MediaIoBaseUpload(
                        fh,
                        mimetype=mime_type,
                        chunksize=1024*1024, 
                        resumable=True
                    )
                    request = drive_service.files().create(
                        body = file_metadata, 
                        media_body=media,
                        fields='id',
                        supportsAllDrives=True
                    )
                    response = None
                    pbar = InitBar(msg)
                    while response is None:
                        status, response = request.next_chunk(num_retries=3)
                        if status:
                            pbar(round(status.progress()*100)) #pbar.percent(status.progress)
                    del pbar


    def get_local_file_hash(self,file_path):
        BUF_SIZE = 65536  # 64kb chunks
        md5 = hashlib.md5()
        with open(file_path, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                md5.update(data)
        return md5.hexdigest()

    def upload_files_recursively(self, drive_service, root_dir, parent_id = None):
        with os.scandir(root_dir) as it:
            for entry in it:
                if entry.is_file():
                    self.create_file(drive_service,os.path.join(root_dir,entry.name),entry.name,parent_id)
                elif entry.is_dir():
                    dir_id = self.create_dir(drive_service,entry.name,parent_id)
                    self.upload_files_recursively(drive_service, os.path.join(root_dir,entry.name),dir_id)


    def download_files_recursively(self, drive_service, file_path, parent_id = None):
        curfiles = self.get_files_in_parent(drive_service, parent_id)
        curfolders = self.get_files_in_parent(drive_service, parent_id, folders=True)

        for file in curfiles:
            cur_path = os.path.join(file_path,file["name"])
            if not file["size"] or file["size"] == "0":
                print("  {} created (or unchanged) as empty file".format(cur_path))
                open(cur_path, 'a').close()
            else:
                if os.path.isfile(cur_path):
                    if self.get_local_file_hash(cur_path) == file["md5Checksum"]:
                        print("  {} exists locally and is unchanged, skipping".format(cur_path))    
                    else:
                        msg = "{} exists locally but is changed, downloading".format(cur_path)
                        self.download_file_to_path(drive_service, file["id"], cur_path, bar_header=msg)
                else:
                    msg = "{} does not exist locally, downloading".format(cur_path)
                    self.download_file_to_path(drive_service, file["id"], cur_path, bar_header = msg)
            
        for folder in curfolders:
            cur_path = os.path.join(file_path,folder["name"])
            if not os.path.isdir(cur_path):
                os.mkdir(cur_path)
            self.download_files_recursively(drive_service, cur_path, folder["id"])

    def download_file_to_path(self, drive_service, file_id, file_path, bar_header=None):
        with io.FileIO(file_path, mode="wb") as fh:
            request = drive_service.files().get_media(
                fileId=file_id,
                supportsAllDrives=True
            )
            downloader = MediaIoBaseDownload(fh, request, chunksize=1024*1024)
            done = False

            pbar = InitBar(bar_header)
            while done is False:
                status, done = downloader.next_chunk(num_retries=3)
                if status:
                    pbar(round(status.progress()*100))
            del pbar
    
    

    def get_files_in_parent(self, drive_service, parent_id, folders=False):
        ret_array = []
        q = "mimeType {} 'application/vnd.google-apps.folder' and '{}' in parents".format("=" if folders else "!=",parent_id)
        
        page_token = None
        while True:
            response = drive_service.files().list(
                q=q,
                spaces='drive',
                fields='nextPageToken, files(id, name, parents, trashed, md5Checksum, size)',
                pageToken=page_token,
                supportsAllDrives=True
            ).execute()
            for file in response.get('files', []):
                if file.get("trashed"):
                    continue
                ret_array.append({
                    "id": file.get("id"),
                    "parents": file.get("parents"),
                    "md5Checksum": file.get("md5Checksum"),
                    "name": file.get("name"),
                    "size": file.get("size")
                })                  
            page_token = response.get('nextPageToken', None)
            if page_token is None:
                break
        return ret_array