# -*- coding: utf-8 -*-

from cliff.command import Command
from datakit.utils import mkdir_p, read_json, write_json
from .gdrive import GDrive
from .project_mixin import ProjectMixin

import os

class Push(Command,ProjectMixin, GDrive):
    "Push data files to gdrive"

    def get_parser(self, prog_name):
        parser = super(Push, self).get_parser(prog_name)
        return parser

    def take_action(self, parsed_args):
        plugin_configs = read_json(self.project_config_path)
        to_write = self.default_configs.copy()
        to_write.update(plugin_configs)

        if 'client_id' in to_write and 'client_secret' in to_write and to_write["client_id"] and to_write["client_secret"]:
            creds = self.auth(to_write)
        else:
            print('You must set client_id and client_secret in the config file')
            return
        to_write.update(creds)

        drive_service = self.get_service(self.get_credentials_from_obj(creds))        
        drive_id = None
        if "drive_id" in to_write and to_write["drive_id"]:
            drive_id = to_write["drive_id"]
        if not "drive_id" in to_write or not to_write["drive_id"]:
            drives = self.get_drives(drive_service)
            if len(drives) > 0:
                self.log.info("Choose the drive to store the data:\n")
                drive_id = self.drive_choice(drives)
                to_write.update({
                    "drive_id": "" if drive_id == None else drive_id
                })

        self.write_config(to_write)
        root_dir = './data'
        
        gdrive_path_arr = to_write["gdrive_path"].split("/")
        
        cur_parent_id = drive_id
        for path_part in gdrive_path_arr:
            project_dir_id = self.create_dir(drive_service, path_part,parent_id=cur_parent_id)
            cur_parent_id = project_dir_id
        
        self.upload_files_recursively(drive_service, root_dir, project_dir_id)
