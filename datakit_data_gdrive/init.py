# -*- coding: utf-8 -*-

import os
import datetime
from cliff.command import Command
from datakit import CommandHelpers
from datakit.utils import mkdir_p, read_json, write_json

from .project_mixin import ProjectMixin

class Init(ProjectMixin, CommandHelpers, Command):

    """
    Initialize a directory for use with Google Drive.
    :Creates:
    * data/ directory (which should be excluded from version control)
    * config/datakit-data-gdrive.json
    """

    def take_action(self, parsed_args):
        self.log.info("Initializing project for Google Drive data integration...")
        dirs_to_create = ['data', 'config']
        [mkdir_p(directory) for directory in dirs_to_create]
        open('data/.gitkeep', 'w').close()
        self.create_project_config()

    def create_project_config(self):
        """Create project config if they don't already exist.
        Plugin-level configs, if configured, will override project defaults.
        """
        now = datetime.datetime.now()
        replacements = {
            "$YEAR": str(now.year),
            "$MONTH": str(now.month).zfill(2),
            "$DAY": str(now.day).zfill(2),
            "$PROJECTSLUG": self.project_slug
        }
        


        if not os.path.exists(self.project_config_path):
            try:
                plugin_configs = read_json(self.plugin_config_path)
                if "gdrive_path" in plugin_configs:
                    for key, value in replacements.items():
                        plugin_configs["gdrive_path"] = plugin_configs["gdrive_path"].replace(key, value)
            except FileNotFoundError:
                plugin_configs = {}
            
            to_write = self.default_configs.copy()
            to_write.update(plugin_configs)

            self.write_config(to_write)
