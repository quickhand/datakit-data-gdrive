# -*- coding: utf-8 -*-
import os
import logging
import json
from datakit.utils import read_json

def ask(question):
    return input(question)

class ProjectMixin:

    "Mixin with code useful across plugin commands"
    log = logging.getLogger(__name__)
    log.setLevel(logging.INFO)

    plugin_slug = 'datakit-data-gdrive'

    @property
    def default_configs(self):
        return {
            'client_id': "",
            'client_secret': "",
            'drive_id': "",
            'gdrive_path': self.project_slug
        }

    @property
    def project_slug(self):
        return os.path.basename(os.getcwd())

    @property
    def project_configs(self):
        try:
            return read_json(self.project_config_path)
        except FileNotFoundError:
            return self.default_configs

    @property
    def project_config_path(self):
        return os.path.join('config', 'datakit-data-gdrive.json')


    def write_config(self,obj):
        with open(self.project_config_path,"w") as fh:
            json.dump(obj, fh, indent=4)
            
    def drive_choice(self,drive_list):
        self.log.info("(1) Default drive")
        for i, drive in enumerate(drive_list):
            self.log.info("({}) {}".format(i+2, drive["name"]))

        choice_msg = "\nType a number or leave blank for default [default]: "

        choice = None
        while not choice:
            str_choice = ask(choice_msg)
            if str_choice.strip() == '':
                choice = 1
            else:
                try:
                    choice = int(str_choice)
                except:
                    pass    
                if choice<1 or choice>len(drive_list)+1:
                    choice = None
        
        return None if choice == 1 else drive_list[choice - 2]["id"]