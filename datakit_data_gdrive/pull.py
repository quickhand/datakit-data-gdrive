# -*- coding: utf-8 -*-

from cliff.command import Command
from datakit.utils import mkdir_p, read_json, write_json
from .gdrive import GDrive
from .project_mixin import ProjectMixin
import os

class Pull(Command,ProjectMixin, GDrive):
    "Pull data files from gdrive"

    def get_parser(self, prog_name):
        parser = super(Pull, self).get_parser(prog_name)
        return parser

    def take_action(self, parsed_args):
        plugin_configs = read_json(self.project_config_path)
        to_write = self.default_configs.copy()
        to_write.update(plugin_configs)

        if 'client_id' in to_write and 'client_secret' in to_write:
            creds = self.auth(to_write)
        else:
            print('You must set client_id and client_secret in the config file')
            return
        to_write.update(creds)
        

        drive_service = self.get_service(self.get_credentials_from_obj(creds)) 
        drive_id = None
        if "drive_id" in to_write and to_write["drive_id"]:
            drive_id = to_write["drive_id"]
        if not "drive_id" in to_write or not to_write["drive_id"]:
            drives = self.get_drives(drive_service)
            if len(drives) > 0:
                self.log.info("Choose the drive where the data is stored:\n")
                drive_id = self.drive_choice(drives)
                to_write.update({
                    "drive_id": "" if drive_id == None else drive_id
                })

        self.write_config(to_write)


        gdrive_path_arr = to_write["gdrive_path"].split("/")
        
        cur_parent_id = drive_id
        for path_part in gdrive_path_arr:
            folder_id, md5 = self.get_file(drive_service, path_part, "application/vnd.google-apps.folder", parent_id=cur_parent_id)
            cur_parent_id = folder_id
        
        if folder_id == None:
            print("Cannot find project folder in Google Drive")
            return
        
        if not os.path.isdir("data"):
            os.mkdir("data")

        self.download_files_recursively(drive_service, "data", folder_id)

