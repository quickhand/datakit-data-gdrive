#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'cliff',
    'datakit-core',
]

test_requirements = [
    'pytest'
]

setup(
    name='datakit-data-gdrive',
    version='0.1.0',
    description="Brief project description.",
    long_description=readme + '\n\n' + history,
    author="Marc Lajoie",
    author_email='lajoiemedia@gmail.com',
    url='https://github.com/quickhand/datakit-data-gdrive',
    packages=[
        'datakit_data_gdrive',
    ],
    package_dir={'datakit_data_gdrive':
                 'datakit_data_gdrive'},
    include_package_data=True,
    entry_points={
        'datakit.plugins': [
            'data-gdrive init= datakit_data_gdrive.init:Init',
            'data-gdrive push= datakit_data_gdrive.push:Push',
            'data-gdrive pull= datakit_data_gdrive.pull:Pull',
        ]
    },
    install_requires=requirements,
    license="ISC license",
    zip_safe=False,
    keywords='datakit-data-gdrive',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ISC License (ISCL)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
