
.. image:: https://img.shields.io/pypi/v/datakit-data-gdrive.svg
        :target: https://pypi.python.org/pypi/datakit-data-gdrive

.. image:: https://img.shields.io/pypi/pyversions/datakit-data-gdrive.svg
        :target: https://pypi.python.org/pypi/datakit-data-gdrive

.. image:: https://img.shields.io/travis/quickhand/datakit-data-gdrive.svg
        :target: https://travis-ci.org/quickhand/datakit-data-gdrive

.. image:: https://readthedocs.org/projects/datakit-data-gdrive/badge/?version=latest
        :target: https://datakit-data-gdrive.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


Overview
========

Brief project description. (Note, not on PyPi yet, so the following is purely aspirational)

* Documentation: http://datakit-data-gdrive.readthedocs.io/en/latest/
* Github: https://github.com/quickhand/datakit-data-gdrive
* PyPI: https://pypi.python.org/pypi/datakit-data-gdrive
* Free and open source software: `ISC license`_

.. _ISC license: https://github.com/quickhand/datakit-data-gdrive/blob/master/LICENSE

Features
========

* TODO

Quickstart
==========

To use this datakit_ plugin::

  $ pip install datakit-data-gdrive

Check out the available commands::

  $ datakit --help

.. note:: See the `datakit-data-gdrive docs`_ for more detailed usage info.


.. _datakit: https://github.com/associatedpress/datakit-core
.. _datakit-data-gdrive docs: https://datakit-data-gdrive.readthedocs.io/en/latest/
